﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.Collections;

/// Thanks for downloading my projectile gun script! :D
/// Feel free to use it in any project you like!
/// 
/// The code is fully commented but if you still have any questions
/// don't hesitate to write a yt comment
/// or use the #coding-problems channel of my discord server
/// 
/// Dave
public class ShootSnowBall : MonoBehaviour
{

    //UI
    public GameObject uIForce;
    public Slider forceVal;
    public Gradient gradientColor;
    public Image fill;
    public TextMeshProUGUI reloadTimeUI;
    float reloadingTime;

    //bullet 
    public GameObject bullet;

    //bullet force
    public float shootForce, upwardForce;
    public float forwardForceTimer;
    public float upwardForceTimer;
    public float resetForces;
    float forwardFTimer, upwardFTimer, resetFTimer;
    public float timer = 0.2f;

    //Gun stats
    public float timeBetweenShooting, spread, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    int bulletsLeft, bulletsShot;
    public bool addForce;

    //Recoil
    public Rigidbody playerRb;
    public float recoilForce;

    //bools
    bool shooting, readyToShoot, reloading;

    //Reference
    public Camera fpsCam;
    public Transform attackPoint;

    //Graphics
    //public GameObject muzzleFlash;
    //public TextMeshProUGUI ammunitionDisplay;

    //bug fixing :D
    public bool allowInvoke = true;
    public bool forceShoot;
    public bool upForce;

    private void Awake()
    {
        reloadTimeUI.gameObject.SetActive(false);
        reloadingTime = reloadTime;
        forwardFTimer = forwardForceTimer;
        upwardFTimer = upwardForceTimer;
        resetFTimer = resetForces;

        fill.color = gradientColor.Evaluate(0f);
        uIForce.SetActive(false);

        //make sure magazine is full
        bulletsLeft = magazineSize;
        readyToShoot = true;

        shootForce = 1;
        upwardForce = 1;
    }

    private void Update()
    {
        if(reloading == true) reloadingTime -= Time.deltaTime;

        forceVal.value = shootForce;
        MyInput();
        fill.color = gradientColor.Evaluate(forceVal.normalizedValue);
        reloadTimeUI.text = reloadingTime.ToString("0.0");
        //Set ammo display, if it exists :D
        //if (ammunitionDisplay != null)
        //    ammunitionDisplay.SetText(bulletsLeft / bulletsPerTap + " / " + magazineSize / bulletsPerTap);
    }
    private void MyInput()
    {
        shooting = Input.GetKeyUp(KeyCode.Mouse0); //if player release left mouse button, shooting is true.

        if (Input.GetKey(KeyCode.Mouse0) && bulletsLeft > 0)
        {
            addForce = true;
        } //if player hold left mouse button, addforce is true.
        else addForce = false; //else is false

        if (addForce) //if addforce is true
        {
            uIForce.SetActive(true);

            //Decrease timers
            forwardFTimer -= Time.deltaTime;
            upwardFTimer -= Time.deltaTime;

            if (forwardFTimer <= 0f) //if the timer for forward force ends.
            {
                if (shootForce < 20)
                {
                    shootForce += 1; //plus 1.
                }
                else if (shootForce >= 20) 
                {
                    ResetForces();
                }

                forwardFTimer = forwardForceTimer; //reset timer
            }
            
            if (upwardFTimer <= 0f) //if the timer for upward force ends.
            {
                if(upwardForce < 10)
                {
                    upwardForce += 1; //plus 1.
                }
                else if(upwardForce >= 10)
                {
                    ResetForces();
                }
                upwardFTimer = upwardForceTimer; //if the timer for upward force ends.
            }
        }
        else //else if addfoce is false
        {
            uIForce.SetActive(false);

            if (shootForce != 1 || upwardForce != 1 || forwardFTimer == forwardForceTimer || upwardFTimer == upwardForceTimer) //check if some variables change
            {
                resetFTimer -= Time.deltaTime; //Timer for reset variables

                if (resetFTimer <= 0)
                {
                    shootForce = 1f;
                    upwardForce = 1f;
                    forwardFTimer = forwardForceTimer;
                    upwardFTimer = upwardForceTimer;
                    resetFTimer = resetForces;
                } //Variables reset
            }
   
            
        }
        

        //Reloading 
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading) Reload();

        ////Reload automatically when trying to shoot without ammo
        //if (readyToShoot && shooting && !reloading && bulletsLeft <= 0) Reload();

        //Shooting
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            //Set bullets shot to 0
            bulletsShot = 0;

            Shoot();
        }
    }

    void ResetForces()
    {
        if(upwardForce >= 10 && shootForce >= 20)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                upwardForce = 1;
                shootForce = 1;
                timer = 0.2f;
            }
        }
    }

    private void Shoot()
    {
        readyToShoot = false;

        //Find the exact hit position using a raycast
        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Just a ray through the middle of your current view
        RaycastHit hit;

        //check if ray hits something
        Vector3 targetPoint;
        if (Physics.Raycast(ray, out hit))
            targetPoint = hit.point;
        else
            targetPoint = ray.GetPoint(75); //Just a point far away from the player

        //Calculate direction from attackPoint to targetPoint
        Vector3 directionWithoutSpread = targetPoint - attackPoint.position;

        //Calculate spread
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        //Calculate new direction with spread
        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0); //Just add spread to last direction

        //Instantiate bullet/projectile
        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity); //store instantiated bullet in currentBullet
        //Rotate bullet to shoot direction
        currentBullet.transform.forward = directionWithSpread.normalized;

        //Add forces to bullet
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);

        //Instantiate muzzle flash, if you have one
        //if (muzzleFlash != null)
        //    Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);

        bulletsLeft--;
        bulletsShot++;

        //Invoke resetShot function (if not already invoked), with your timeBetweenShooting
        if (allowInvoke)
        {
            Invoke(nameof(ResetShot), timeBetweenShooting);
            allowInvoke = false;

            //Add recoil to player (should only be called once)
            playerRb.AddForce(-directionWithSpread.normalized * recoilForce, ForceMode.Impulse);
        }

        //if more than one bulletsPerTap make sure to repeat shoot function
        if (bulletsShot < bulletsPerTap && bulletsLeft > 0)
            Invoke("Shoot", timeBetweenShots);
    }



    private void ResetShot()
    {
        //Allow shooting and invoking again
        readyToShoot = true;
        allowInvoke = true;
    }

    private void Reload()
    {
        reloadTimeUI.gameObject.SetActive(true);
        
        reloading = true;
        Invoke("ReloadFinished", reloadTime); //Invoke ReloadFinished function with your reloadTime as delay
    }
    private void ReloadFinished()
    {
        //Fill magazine
        bulletsLeft = magazineSize;
        reloading = false;
        reloadingTime = reloadTime;
        reloadTimeUI.gameObject.SetActive(false);
    }
}
