﻿#region Usings
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;
#endregion

[RequireComponent(typeof(NavMeshAgent)), RequireComponent(typeof(Rigidbody)), RequireComponent(typeof(EnemyAttack))]
public class Enemy : MonoBehaviour
{
    #region Enemy AI
    #region Assignables
    [Header("Assignables")]
    //[HideInInspector]
    public NavMeshAgent agent;
    public Transform player;
    public LayerMask whatIsGround, whatIsPlayer;
    #endregion

    //public float health;
    [Space(height: 20)]

    #region Hide Mechanic
    [Header("Hide Mechanic")]
    public GameObject[] hidePoint;
    public GameObject actualHidePoint;
    bool crouch; //Sin funcion actual
    #endregion

    #endregion

    #region Bools
    [Header("Bools")]
    public bool attack = false;
    public bool reload = false;
    public bool move = false;
    #endregion

    #region Timers
    [Header("Timers")]
    public float moveTime;
    public float delayTimer;
    #endregion

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        #region AI
        player = GameObject.Find("FirstPerson-AIO").transform;
        delayTimer = 10f;
        hidePoint = GameObject.FindGameObjectsWithTag("HidePoint");
        #endregion

        moveTime = 7f;
    }

    private void Update()
    {
        if (delayTimer <= 0 && move == false)
        {
            delayTimer = 0f;

            moveTime -= Time.deltaTime;
            if (moveTime <= 0)
            {
                move = true;
            }
        }
        else if (delayTimer > 0 && move == false)
        {
            delayTimer -= Time.deltaTime;
        }
        //if(move == true && agent.velocity.magnitude < 0.15f)
        //{
        //    move = false;

        //}


        if (move == true)
        {

            ChangeHidePoint();
        }

        if (agent.velocity.magnitude >= 0.1f)
            move = true;
        else
            move = false;


    }

    private void ChangeHidePoint()
    {
        
        Debug.LogError("Cambio de Pos");
        if(delayTimer <= 0)
        {
            moveTime = Random.Range(7, 20); //Resetea el timer.
            int i = Random.Range(0, hidePoint.Length); //Toma un "Hide Point" random.
            actualHidePoint = hidePoint[i]; //Setea el "Hide Point" seleccionado como el objetivo de movimiento.
        }

        delayTimer = 10f;
        agent.SetDestination(actualHidePoint.transform.position); //Va al "Hide Point".

    }
}
