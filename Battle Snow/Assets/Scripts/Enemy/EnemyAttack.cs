﻿#region Usings
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;
#endregion

public class EnemyAttack : MonoBehaviour
{
    #region Assignables
    [Header("Assignables")]
    public GameObject canon;
    public GameObject bullet;
    GameObject target;
    #endregion

    #region Gun Stats
    [Header("Gun Stats")]
    public float shootForce, upwardForce;
    public float timeBetweenShooting, spread, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public int bulletsLeft, bulletsShot;
    float reloadingTime;
    #endregion

    #region Recoil
    [Header("Recoil")]
    public Rigidbody enemyRb;
    public float recoilForce;
    #endregion

    #region Bug Fixing
    [HideInInspector]
    public bool allowInvoke = true;
    #endregion

    #region Timers
    [Header("Timers")]
    public float attackTimer;
    public float reloadTimer;
    #endregion

    #region Bools
    [Header("Bools")]
    public bool attack = false;
    public bool reload = false;
    #endregion

    Vector3 velocity;

    private void Start()
    {
        attackTimer = 5f;
        bulletsLeft = magazineSize;
        reloadingTime = reloadTimer;
    }

    private void Update()
    {
        target = GameObject.Find("FirstPerson-AIO");
        if(this.GetComponent<Enemy>().move == false)
        {
            Look();
            Debug.LogError("Can look");
        }
       

    }

    void Look()
    {
        transform.LookAt(target.transform);
        Debug.DrawLine(transform.position, target.transform.position, Color.red);
        if (bulletsLeft >= 1)
        {
            attack = true;
            reload = false;
            reloadingTime = reloadTimer;
            if (attack == true) 
            {
                attackTimer -= Time.deltaTime;
            }
            
            if (attackTimer <= 0)
            {
                Shoot();
            }
        }
        else if (bulletsLeft <= 0)
        {
            reload = true;
            attackTimer = 5f;
            if (reload == true)
            {
                attack = false;
                Reload();
            }
        }
    }

    private void Shoot()
    {
        attack = false;

        //Calculate spread
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        Vector3 direction = canon.transform.forward + new Vector3(x, y, 0);

        //Find the exact hit position using a raycast
        //Ray ray = direction; //Just a ray through the middle of your current view
        RaycastHit hit;

        //check if ray hits something
        Vector3 targetPoint;
        if (Physics.Raycast(canon.transform.position, direction, out hit))
            targetPoint = hit.point;
        else
            targetPoint = direction; //Just a point far away from the player

        //Calculate direction from attackPoint to targetPoint
        Vector3 directionWithoutSpread = targetPoint - canon.transform.position;

        //Calculate new direction with spread
        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0); //Just add spread to last direction

        //Instantiate bullet/projectile
        GameObject currentBullet = Instantiate(bullet, canon.transform.position, Quaternion.identity); //store instantiated bullet in currentBullet
        //Rotate bullet to shoot direction
        currentBullet.transform.forward = directionWithSpread.normalized;

        //Add forces to bullet
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(canon.transform.up * upwardForce, ForceMode.Impulse);

        //Instantiate muzzle flash, if you have one
        //if (muzzleFlash != null)
        //    Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);

        bulletsLeft--;
        bulletsShot++;

        //Invoke resetShot function (if not already invoked), with your timeBetweenShooting
        if (allowInvoke)
        {
            Invoke(nameof(ResetShot), reloadingTime);
            allowInvoke = false;

            //Add recoil to player (should only be called once)
            enemyRb.AddForce(-directionWithSpread.normalized * recoilForce, ForceMode.Impulse);
        }
        attackTimer = 5;

        //if more than one bulletsPerTap make sure to repeat shoot function
        if (bulletsShot < bulletsPerTap && bulletsLeft > 0)
            Invoke("Shoot", attackTimer);
    }

    private void ResetShot()
    {
        //Allow shooting and invoking again
        attack = true;
        allowInvoke = true;
    }

    private void Reload()
    {
        reload = true;
        Invoke(nameof(ReloadFinished), reloadTimer); //Invoke ReloadFinished function with your reloadTime as delay
    }

    private void ReloadFinished()
    {
        //Fill magazine
        bulletsLeft = magazineSize;
        reload = false;
        reloadingTime = reloadTimer;
    }
}
