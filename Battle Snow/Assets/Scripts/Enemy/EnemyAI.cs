﻿#region Usings
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;
#endregion

#region Require Components
[RequireComponent(typeof(NavMeshAgent)), RequireComponent(typeof(Rigidbody))]
#endregion

public class EnemyAI : MonoBehaviour
{
    
    #region Enemy AI
    #region Assignables
    [Header("Assignables")]
    //[HideInInspector]
    public NavMeshAgent agent;
    //public Transform player;
    public LayerMask whatIsGround, whatIsPlayer;
    #endregion

    //public float health;
    [Space(height: 20)]

    bool crouch;

    #region Hide Mechanic
    [Header("Hide Mechanic")]
    public GameObject[] hidePoint;
    public GameObject actualHidePoint;
    #endregion

    #endregion

    #region Bools
    [Header("Bools")]
    public bool attack = false;
    public bool reload = false;
    public bool move = false;
    #endregion

    #region Timers
    [Header("Timers")]
    public float attackTimer;
    public float reloadTimer;
    public float hPointResetTime;
    public float delayTimer;
    #endregion

    #region Gun
    #region Assignables
    [Header("Assignables")]
    public GameObject canon;
    public GameObject bullet;
    GameObject target;
    #endregion



    #region Gun Stats
    [Header("Gun Stats")]
    public float shootForce, upwardForce;
    public float timeBetweenShooting, spread, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public int bulletsLeft, bulletsShot;
    float reloadingTime;
    #endregion



    #region Recoil
    [Header("Recoil")]
    public Rigidbody enemyRb;
    public float recoilForce;
    #endregion



    #region Bug Fixing
    [HideInInspector]
    public bool allowInvoke = true;
    #endregion

    #endregion

    Vector3 velocity;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        velocity = agent.velocity;
        #region AI
        //player = GameObject.Find("FirstPerson-AIO").transform;
        
        delayTimer = 10f;
        hidePoint = GameObject.FindGameObjectsWithTag("HidePoint");
        #endregion

        hPointResetTime = 7f;

        #region Gun
        bulletsLeft = magazineSize;
        //bulletsLeft = 1;
        //attack = true;

        //shootForce = 1;
        //upwardForce = 1;
        #endregion
    }

    private void Update()
    {

        #region Gun

        target = GameObject.Find("FirstPerson-AIO");

        
        #endregion

        //Bools();


        if (delayTimer <= 0)
        {
            delayTimer = 0f;
         
            hPointResetTime -= Time.deltaTime;
            if (hPointResetTime <= 0)
            {
                move = true;
            }
        }
        else if (delayTimer > 0)
        {
            delayTimer -= Time.deltaTime;
        }
        //if(move == true && agent.velocity.magnitude < 0.15f)
        //{
        //    move = false;

        //}


        if (move == false)
        {
            transform.LookAt(target.transform);
            Debug.DrawLine(transform.position, target.transform.position, Color.red);
            if (bulletsLeft != 0)
            {
                attack = true;
            }
            else if (bulletsLeft == 0 && agent.velocity.magnitude < 0.15f)
            {
                reload = true;
            }
        }

        Bools();






        

    }

    void Bools()
    {
        if(move == true)
        {
            attack = false;
            reload = false;
            attackTimer = 5f;
            reloadingTime = 2f;
            ChangeHidePoint();
        }
        else
        {
            if(attack == true)
            {
                reload = false;
                reloadingTime = 2f;
                attackTimer -= Time.deltaTime;
                if (attackTimer <= 0) 
                {
                    Shoot();
                    agent.velocity = velocity;
                }
                //Invoke(nameof(Shoot), attackTimer);
                
            }
            else
            {
                attackTimer = 5f;
                if(reload == true)
                {
                    attack = false;
                    move = false;
                    Reload();
                }
            }
        }
    }
   
    private void Shoot()
    {
        attack = false;

        //Calculate spread
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        Vector3 direction = canon.transform.forward + new Vector3(x, y, 0);

        //Find the exact hit position using a raycast
        //Ray ray = direction; //Just a ray through the middle of your current view
        RaycastHit hit;

        //check if ray hits something
        Vector3 targetPoint;
        if (Physics.Raycast(canon.transform.position, direction, out hit))
            targetPoint = hit.point;
        else
            targetPoint = direction; //Just a point far away from the player

        //Calculate direction from attackPoint to targetPoint
        Vector3 directionWithoutSpread = targetPoint - canon.transform.position;

        //Calculate new direction with spread
        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0); //Just add spread to last direction

        //Instantiate bullet/projectile
        GameObject currentBullet = Instantiate(bullet, canon.transform.position, Quaternion.identity); //store instantiated bullet in currentBullet
        //Rotate bullet to shoot direction
        currentBullet.transform.forward = directionWithSpread.normalized;
        agent.velocity = velocity;

        //Add forces to bullet
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(canon.transform.up * upwardForce, ForceMode.Impulse);

        //Instantiate muzzle flash, if you have one
        //if (muzzleFlash != null)
        //    Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);

        bulletsLeft--;
        bulletsShot++;

        //Invoke resetShot function (if not already invoked), with your timeBetweenShooting
        if (allowInvoke)
        {
            Invoke(nameof(ResetShot), reloadingTime);
            allowInvoke = false;

            //Add recoil to player (should only be called once)
            enemyRb.AddForce(-directionWithSpread.normalized * recoilForce, ForceMode.Impulse);
        }
        attackTimer = 5;
        
        //if more than one bulletsPerTap make sure to repeat shoot function
        if (bulletsShot < bulletsPerTap && bulletsLeft > 0)
            Invoke("Shoot", attackTimer);
    }

    private void ResetShot()
    {
        //Allow shooting and invoking again
        attack = true;
        allowInvoke = true;
    }

    private void Reload()
    {
        reload = true;
        Invoke("ReloadFinished", reloadTime); //Invoke ReloadFinished function with your reloadTime as delay
    }

    private void ReloadFinished()
    {
        //Fill magazine
        bulletsLeft = magazineSize;
        reload = false;
        reloadingTime = reloadTime;
    }

    private void ChangeHidePoint()
    {
        Debug.LogError("Cambio de Pos");
        hPointResetTime = Random.Range(7, 20); //Resetea el timer.
        
        delayTimer = 10f;
        int i = Random.Range(0, hidePoint.Length); //Toma un "Hide Point" random.
        actualHidePoint = hidePoint[i]; //Setea el "Hide Point" seleccionado como el objetivo de movimiento.

        agent.SetDestination(actualHidePoint.transform.position); //Va al "Hide Point".

    }
}
